//Express
const express = require('express');

//path
const path = require('path');

//using express
const app = express();

//set the static path
app.use(express.static(path.join(__dirname, "client")));

const port = 3000;
app.listen(port, () => {
    console.log(`server started on ${port}`);
});