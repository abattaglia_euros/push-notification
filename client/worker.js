self.addEventListener("push", e => {
    const data = e.data.json();

    sendNotification(data.title, {
        body: data.body, //the body of the push notification
        image: data.image,
        icon: data.icon, // icon 
    });
});

function sendNotification(title, props) {
    self.registration.showNotification(title, props);
}